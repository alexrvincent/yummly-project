/* Given an array of recipes, randomly orders them inline and returns the same array */
function shuffleRecipes(recipes) {

    var currIndex = recipes.length;
    var tempValue;
    var randomIndex;

    // As long as the current index does not reach zero 
    while (0 !== currIndex) {

        // Get a random index and
        randomIndex = Math.floor(Math.random() * currIndex);
        currIndex -= 1;

        // Swap the values at the current index with the one at the random index
        tempValue = recipes[currIndex];
        recipes[currIndex] = recipes[randomIndex];
        recipes[randomIndex] = tempValue;

    }

    return recipes;
}

/* Clears the current recipe list and updates it with the array of HTML nodes representing the new recipe order*/
function updateRecipes(recipeRoot, newRecipes) {
    recipeRoot.innerHTML = '';
    for (var recipe in newRecipes) {
        recipeRoot.appendChild(newRecipes[recipe]);
    }
}

/* First get the recipes markup in JavaScript so we can manipulate it as we like */
var ingredientUrl = "https://www.yummly.com/recipes?allowedIngredient="
var nameSorted = false;
var calorieSorted = false;
var rootList = document.querySelector('ul');
var recipes = Array.prototype.slice.call(document.getElementsByClassName('recipe'));

/* Next, modify the recipe titles to be anchor tags in order to differentiate them from the span buttons */
for (var recipe in recipes) {

    // Select the span tag name
    var recipeTitle = recipes[recipe].children[1];

    // Create the new anchor tag to live inside of it
    var anchorTitle = document.createElement("a");
    anchorTitle.href = "https://www.yummly.com/recipe/" + recipeTitle.innerText.split(" ").join("-") + "-" + recipes[recipe].dataset.id;
    anchorTitle.target = "_blank"
    anchorTitle.innerText = recipeTitle.innerText;

    // Replace the span inner text with the new anchor name
    recipeTitle.innerHTML = "";
    recipeTitle.appendChild(anchorTitle);
}


/* ---- Event Listeners ---- */

/* Randomize Button - shuffle the array of recipes and update the list */
document.querySelector("[data-order='random']").addEventListener("click", function () {
    shuffleRecipes(recipes);
    nameSorted = false;
    calorieSorted = false;
    updateRecipes(rootList, recipes);
})

/* Order Name Button - sort the recipes alphabetically (unless they're sorted then reverse sort them!) */
document.querySelector("[data-order='name']").addEventListener("click", function () {
    if (!nameSorted) {
        recipes = recipes.sort(function (a, b) {
            return a.children[1].innerText.localeCompare(b.children[1].innerText);
        })
        nameSorted = true;
    }
    else {
        recipes.reverse();
        nameSorted = false;
    }
    calorieSorted = false;
    updateRecipes(rootList, recipes);
})

/* Order Calories Button - sort the recipes by increasing calories (unless they're sorted then reverse sort them!) */
document.querySelector("[data-order='calories']").addEventListener("click", function () {
    if (!calorieSorted) {
        recipes = recipes.sort(function (a, b) {
            if (parseInt(a.children[0].innerHTML) < parseInt(b.children[0].innerHTML)) return -1;
            else if (parseInt(a.children[0].innerHTML) > parseInt(b.children[0].innerHTML)) return 1;
            else return 0;
        })
        calorieSorted = true;
    }
    else {
        recipes.reverse();
        calorieSorted = false;
    }
    nameSorted = false;
    updateRecipes(rootList, recipes)
})

/* Recipe item - to avoid multiple event listeners, we use event delegation to identify the item that was clicked 
   and update the DOM or open a link;
*/
rootList.addEventListener("click", function (event) {

    var active = "ingredients ingredients--active";
    var inactive = "ingredients";

    // Upon clicking on a blue rectangle (recipe span), swap its css classes to animate show/hiding it
    if (event.target.tagName === 'SPAN' && (event.target.parentElement.className === 'recipe')) {

        if (event.target.parentElement.children[2].className === active) {
            event.target.parentElement.children[2].className = inactive;
        }
        else {
            event.target.parentElement.children[2].className = active;
        }
    }

    // Case: Clicking an ingredient item with no amount should still open the ingredient page on Yummly.
    else if (event.target.tagName === "LI" && event.target.children.length === 0) {
        window.open(ingredientUrl + event.target.childNodes[0].data, "_blank");
    }

    // Case: Clicking an ingredient item (with an amount) should open the ingredient page on Yummly .
    else if (event.target.tagName === "LI" && event.target.children[0].className === 'amount') {
        window.open(ingredientUrl + event.target.childNodes[1].data, "_blank");
    }

    // Case: Clicking an ingredient amount should still open the ingredient page on Yummly.
    else if (event.target.tagName === "SPAN" && event.target.className === 'amount') {
        window.open(ingredientUrl + event.target.parentElement.childNodes[1].data, "_blank");
    }

})
