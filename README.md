# yummly-project

Please see attached the completed project. Simply run the index.html file in a browser.

I've attached a css/style.css and js/script.js files for clarity. Because the specs requested
all code go between the style / script tag, I placed them there. In reality, I would develop them
in the separate files and load them in to keep the HTML sparse. If allowed to use ES6 modules or additional scripts, I would
even split out my utility functions into separate files and unit test them appropriately. Lots of room for
improvement here!

The code was tested in all major browsers (Chrome, Firefox, Safari, Edge) and was written to be as
backwards compatible as possible (i.e: No ES6 features :( )

Please let me know if you have any questions concerning the implementation. I tried my best
to keep the code as tight, clean, and commented as possible but I'm always open to feedback.

Thanks!